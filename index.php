<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL,'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl); 
curl_close($curl);

$result = json_decode($result, true);

$global = $result['Global'];
$temp_NewConfirmed = (string)$global['TotalConfirmed'];
$temp_TotalConfirmed = (string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$temp_TotalDeaths = (string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$temp_TotalRecovered = (string)$global['TotalRecovered'];
$countries = $result['Countries'];

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="shortcut icon" href="virus.png" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Dashboard</title>
    <style>
        #nav{
            color: white;
        }
        .country{
            overflow-x : hidden;
            overflow-y: auto;
            height: 750px;
        }
        #icon{
            background: red; 
        }
        .card-body-icon {
            position: absolute;
            z-index: 0;
            top: 25px;
            right: 1px;
            opacity: 0.4;
            font-size: 90px;
        }
    </style>
</head>
<body style="background-image:linear-gradient(to top, #d9d9d9, #000000);">       
    <!-- Just an image -->
    <nav class="navbar navbar-expand-lg navbar-light bg-dark" >
        <a class="navbar-brand" href="#">
            <img src="virus.png" width="50px" height="50px" alt="" loading="lazy">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav text-light">
                <a class="nav-link active text-light" href="index.php">Home <span class="sr-only">(current)</span></a>
                <a class="nav-link text-light" href="peta.php">Lihat Peta</a>
            </div>
        </div>
    </nav>
    
    <!--  -->
    <div class="container">
        <div class="row ">
            <div class="col">
                <h3 class="py-3 text-light" style="text-align:center;"><b>Global Coronavirus Disease (COVID-19) Dasboard</b></h3>
            </div>
        </div>
    </div>
    <div class="container mt-3">
        <div class="row" >
            <div class="col-lg-12 col-sm-12">
                <center>
                    <h6 class="text-light">Update Globally, as of <?php echo $countries[0]['Date']; ?></h6>
                </center>
            </div>
        </div>
    </div>
    
    
    <div class="container mt-5">
        <div class="row border p-3" style="background-image:linear-gradient(to top, #000000);" id="content">
            <div class="col-12 col-lg-4 col-sm-12 col-md-12 mt-2">
                <div class="card text-center">
                    <div class="card-body text-light"  style="background-image:linear-gradient(to top, #ff3333, #ffff1a);">
                        <h5>Total Confirmed</h5>
                        <div class="card-body-icon">
                            <i class="fas fa-hospital-user mr-2"></i>
                        </div>
                        <h4><?php echo number_format($temp_TotalConfirmed);?>  <i>cases</i></h4> 
                        <h6 class="card-text">New Confirmed</h6>
                        <p><?php echo number_format($temp_NewConfirmed);?>  <i>cases</i></p>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-12 col-lg-4  col-sm-12 col-md-12 mt-2">
                <div class="card text-center">
                    <div class="card-body text-light" style="background-image:linear-gradient(to top,#ff0000, #00e64d);">
                        <h5>Total Deaths</h5>
                        <div class="card-body-icon">
                        <i class="fas fa-user-slash mr-2"></i>
                        </div>
                        <h4 class="card-title"><?php echo number_format($temp_TotalDeaths);?>  <i>cases</i></h4>
                        <h6 class="card-text">New Deaths</h6>
                        <p><?php echo number_format($temp_NewDeaths);?>  <i>cases</i></p>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-12 col-lg-4  col-sm-12 col-md-12 mt-2">
                <div class="card text-center">
                    <div class="card-body text-light" style="background-image:linear-gradient(to top, #ffff66, #00e64d);">
                        <h5>Total Recovered</h5>
                        <div class="card-body-icon">
                            <i class="fas fa-virus-slash mr-2"></i>
                        </div>
                        <h5 class="card-title"><?php echo number_format($temp_TotalRecovered );?>  <i>cases</i></h5> 
                        <h6 class="card-text">New Recovered</h6>
                        <p><?php echo number_format($temp_TotalRecovered);?>  <i>cases</i></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-3">
        <div class="row border p-3" style="background-image:linear-gradient(to top,  #d9d9d9);">
            <div class="col-lg-3 col-sm-12 col-md-12">
                <div class="card-header bg-secondary text-light text-center">Country</div>
                <div class="country">
                    <?php foreach($countries as $key=>$value):?>
                    <ul  class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $value['Country'];?>
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdown">
                                <p class="dropdown text-white px-2"> New Confirmed  :  <?php echo $value['NewConfirmed'];?></p>
                                <p class="dropdown text-white px-2"> Total Confirmed  :  <?php echo $value['TotalConfirmed'];?></p>
                                <p class="dropdown text-white px-2"> New Deaths     :  <?php echo $value['NewDeaths'];?></p>
                                <p class="dropdown text-white px-2"> Total Deaths     :  <?php echo $value['TotalDeaths'];?></p>
                                <p class="dropdown text-white px-2"> New Recovered  :  <?php echo $value['NewRecovered'];?></p>
                                <p class="dropdown text-white px-2"> Total Recovered  :  <?php echo $value['TotalRecovered'];?></p>
                            </div>
                        </li>
                    </ul>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-lg-9 col-sm-12 col-md-12">
                <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Confirmed Covid-19</h4>
                <div class="d-flex justify-content-center">
                    <canvas width="200" height="200" id="confirm"></canvas>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-md-12">
                <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Death Covid-19</h4>
                <div class="d-flex justify-content-center">
                    <canvas id="death" class="mt-3"></canvas>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-md-12">
                <h4 style="text-align: center; font-size: 2vw;" class="mt-3">Data Recovery Covid-19</h4>
                <div class="d-flex justify-content-center">
                    <canvas id="recovery" class="mt-3"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->

<script>
    var confirmed = document.getElementById('confirm').getContext('2d');
    var death = document.getElementById('death').getContext('2d');
    var recovery = document.getElementById('recovery').getContext('2d');
    
    var data = $.ajax({
        url: "https://api.covid19api.com/summary",
        cache: false
    })
    .done(function (covid) {
        
        function getCountries(covid) {
            var temp_country=[];
            
            covid.Countries.forEach(function(el){
                temp_country.push(el.Country);
            })
            return temp_country;
        }
        
        function getConfirmed(covid) {
            var temp_confirmed=[];
            
            covid.Countries.forEach(function(el) {
                temp_confirmed.push(el.TotalConfirmed)
            })
            return temp_confirmed;
        }
        
        function getDeath(covid) {
            var temp_death = [];
            
            covid.Countries.forEach(function(el) {
                temp_death.push(el.TotalDeaths)
            })
            return temp_death;
        }
        
        function getRecovery(covid) {
            var temp_recovery = [];
            
            covid.Countries.forEach(function(el) {
                temp_recovery.push(el.TotalRecovered)
            })
            return temp_recovery;
        }
        
        var colors = [];
        function getRandomColor() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        }     
        
        for (var i in covid.Countries) {
            colors.push(getRandomColor());
        }
        
        var confirmPieChart = new Chart(confirmed,{
            type: 'bar',
            data: {
                labels: getCountries(covid),
                datasets: [{
                    backgroundColor: colors,
                    label: '# Total',
                    data: getConfirmed(covid),
                    borderWidth: 0.5
                }]
            }, 
            options: {
                legend: {
                    display: false,
                }
            }
        })
        
        var deathPieChart = new Chart(death,{
            type: 'doughnut',
            data: {
                labels: getCountries(covid),
                datasets: [{
                    backgroundColor: colors,
                    label: '# Total',
                    data: getDeath(covid),
                    borderWidth: 0
                }]
            }, 
            options: {
                legend: {
                    display: false,
                }
            }
        })
        
        var recoveryPieChart = new Chart(recovery,{
            type: 'doughnut',
            data: {
                labels: getCountries(covid),
                datasets: [{
                    backgroundColor: colors,
                    label: '# Total',
                    data: getRecovery(covid),
                    borderWidth: 0
                }]
            }, 
            options: {
                legend: {
                    display: false,
                }
            }
        })
    });
</script>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
-->
</body>
</html>